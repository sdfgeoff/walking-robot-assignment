%% Calculate the tool location from the joint angles
function [pos Transforms] = forward_kinematics(bot, pose)
    % The first return value is the end effector position for the
    % given pose. The remaining matrices are the transformations to 
    % each link.
    theta1 = pose(1);
    theta2 = pose(2);
    theta3 = pose(3);
    T1 = Rz(theta1)*Tz(bot.d(1))*Tx(bot.a(1))*Rx(bot.alpha(1));
    T2 = Rz(theta2)*Tz(bot.d(2))*Tx(bot.a(2))*Rx(bot.alpha(2));
    T3 = Rz(theta3)*Tz(bot.d(3))*Tx(bot.a(3))*Rx(bot.alpha(3));
    T1_0 = T1;
    T2_0 = T1 * T2;
    T3_0 = T1 * T2 * T3;
    Transforms = {eye(4), T1_0, T2_0, T3_0};
    pos = T3_0;
end



%% Generate a transformation matrix with a rotation around the Z axis
function R = Rz(theta)
    R = [cos(theta) -sin(theta) 0 0;
         sin(theta) cos(theta)  0 0;
         0          0           1 0;
         0          0           0 1];
end
%% Generate a transformation matrix with a rotation around the X axis
function R = Rx(theta)
    R = [1 0          0           0
         0 cos(theta) -sin(theta) 0;
         0 sin(theta) cos(theta)  0;
         0 0          0           1];
end
%% Generate a transformation matrix with a displacement on the Z axis
function T = Tz(d)
    T = [1 0 0 0;
         0 1 0 0;
         0 0 1 d;
         0 0 0 1];
end
%% Generate a transformation matrix with a displacement on the X axis
function T = Tx(a)
    T = [1 0 0 a;
         0 1 0 0;
         0 0 1 0;
         0 0 0 1];
end