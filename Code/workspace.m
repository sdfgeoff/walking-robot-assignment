function workspace(bot, angle_min, angle_max, step_size)
    theta_1s = angle_min(1):step_size:angle_max(1);
    theta_2s = angle_min(2):step_size:angle_max(2);
    theta_3s = angle_min(3):step_size:angle_max(3);
    total = length(theta_1s) * length(theta_2s) * length(theta_3s)
    all_pos = zeros(3, total);
    color = zeros(1, total);
    
    i = 1;
    for theta_1 = theta_1s;
        for theta_2 = theta_2s;
            for theta_3 = theta_3s;
                thetas = [theta_1, theta_2, theta_3];
                pos = forward_kinematics(bot, thetas);
                all_pos(:,i) = pos(1:3,4);
                color(:,i) = near_singularity(bot, thetas);
                i = i + 1;
            end
        end
    end
    scatter3(all_pos(1,:), all_pos(2,:), all_pos(3,:), [], color(1,:), '.')
end


function val = near_singularity(bot, thetas)
    jac = jacobian(bot, thetas);
    val = cond(jac);
end
