%% Procedurally generates and animates a walk cycle along the passed in direction vector
function walk(bot, rest_pos, dir_vec)
    time_step = 0.05;
    time = 0:time_step:5;
    current_pos = [0.025, 0.05, -0.075];
    in_air = 0;
    pose_list = zeros(length(time), 3);
    for t = 1:length(time)
        
        %Evaluate how far away the foot is from it's home - with a bias
        %towards where the robot is going
        comfort = norm(current_pos(1:2)+dir_vec(1:2)*2/3 - rest_pos(1:2));
        
        %Figure out which way the foot is going
        if in_air == 0
            height = rest_pos(3);
            dir = dir_vec;
            if (comfort > 0.08)
                in_air = 1;
            end
        else 
            height = rest_pos(3) + 0.02;
            dir = -dir_vec;
            if (comfort < 0.01)
                in_air = 0;
            end
        end
        
        %Move the foot
        current_pos = current_pos + dir*time_step; %X and Y from direction
        current_pos(3) = (current_pos(3) + (height))/2; %With smoothing on foot height
        
        
        pose = inverse_kinematics(bot, current_pos);
        pose_list(t,:) = pose;
    end
    bot.plot(pose_list, 'trail', '-' );

end