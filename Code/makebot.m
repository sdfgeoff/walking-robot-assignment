%% Creates a robotics toolkit robot from a table of DH parameters
function bot = makebot(dh_table, name)
    num_links = size(dh_table);
    num_links = num_links(1);
    rest_pose = zeros(num_links, 0);
    for i = [1:num_links]
        data = dh_table(i, :);
        new_link = Link('d', data(2), 'a', data(3), 'alpha', data(4));
        rest_pose = [rest_pose data(1)];
        if i == 1
            bot = SerialLink(new_link);
        else
           bot = bot * SerialLink(new_link); % Multiplying robots joins them!
        end
    end
    bot.name = name;
end