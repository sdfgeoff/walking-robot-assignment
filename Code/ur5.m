clear;
clc;
%
%ROBOT_NAME = 'Leg';
%DH_TABLE = [
%    %D,    a,    alpha
%    [0, 1, pi/2];
%    [0, 1, 0];
%];
%
%
%num_links = size(DH_TABLE);
%num_links = num_links(1);
%links = {};
%for i = [1:num_links]
%   data = DH_TABLE(i, :);
%   new_link = Link('d', data(1), 'a', data(2), 'alpha', data(3));
%   links = [links {new_link}];
%end
%bot = SerialLink(new_link, 'name', ROBOT_NAME);

L1 = Link('d', 0.0892,       'a', 0,     'alpha', pi/2);
L2 = Link('d', 0,       'a', 0.425, 'alpha', 0);
L3 = Link('d', 0,       'a', 0.392, 'alpha', 0);
L4 = Link('d', 0.1093,       'a', 0.0, 'alpha', pi/2);
L5 = Link('d', 0.09475,       'a', 0.0, 'alpha', pi/2);
L6 = Link('d', -0.0825,       'a', 0.0, 'alpha', pi);

bot = SerialLink([L1  L2  L3 L4 L5 L6], 'name', 'UR5');
        bot.plot([0 pi/3 -pi/2 pi-pi/3 pi/2 0.0], 'floorlevel', 0)