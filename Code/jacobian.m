%% Calculate the jacobian for a given position
function jacob = jacobian(bot, thetas)
    [pos Transforms] = forward_kinematics(bot, thetas);
    jacob = zeros(6, length(Transforms)-1);
    for joint_id = 1:length(Transforms)-1
        joint_transform = cell2mat(Transforms(joint_id));
        z_vec = joint_transform(1:3,3);
        z_hat_x = [0         -z_vec(3)   z_vec(2) ;
                   z_vec(3)   0         -z_vec(1) ;
                  -z_vec(2)   z_vec(1)   0       ];
        p_vec = joint_transform(1:3,4);
        radius_vec = pos(1:3,4) - p_vec;
        velocity = z_hat_x * radius_vec;
        omega = z_vec;
        jacob(:,joint_id) = [velocity;omega];
    end
end