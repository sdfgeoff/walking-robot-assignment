%% Calculate the angles given a desired tool location
function pose = inverse_kinematics(bot, pos)
    x = pos(1);
    y = pos(2);
    z = -pos(3);
    r_dash = sqrt(x^2 + y^2);
    sigma = asin((bot.d(2) + bot.d(3))/r_dash);
    theta1 = atan2(y, x) + sigma;
    r = sqrt(r_dash^2 - (bot.d(2) + bot.d(3))^2);
    
    L = sqrt((r - bot.a(1))^2 + z^2);
    Beta = atan2(z, r - bot.a(1));
    theta2 = acos((bot.a(2)^2 + L^2 - bot.a(3)^2)/(2 * bot.a(2) * L)) - Beta;
    theta3 = -pi + acos((bot.a(2)^2 + bot.a(3)^2 - L^2)/(2*bot.a(2)*bot.a(3)));
    pose = [theta1, theta2, theta3];
end