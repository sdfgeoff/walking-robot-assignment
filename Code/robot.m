function robot()
    ROBOT_NAME = 'SingleLeg';
    DH_TABLE = [
        %theta,  D,      a,      alpha
        0,      0.0,     0.01,    pi/2;
        0,      0.020,    0.04,    0;
        -pi/2   0.005,    0.075,    0;
    ];
    THETA_MIN = [-pi/2,-pi/2,-pi];
    THETA_MAX = [pi/2,pi/2,-pi/4];
    bot = makebot(DH_TABLE, ROBOT_NAME);
    
    rest_pos = [0.025, 0.05, -0.075];
    thetas = inverse_kinematics(bot, rest_pos);
    %bot.plot(THETA_MIN, 'trail', '-', 'floorlevel', rest_pos(3)); 
    %jacobian(bot, thetas)
    %walk(bot, rest_pos, [0.0, 0.1, 0]);
    workspace(bot, THETA_MIN, THETA_MAX, 0.05);
end