import bge
import mathutils
import math

LIN_SPEED = 1.0
ROT_SPEED = 0.5
LEG_SPEED = 5.0
PICK_UP_EFFORT = 0.2
PUT_DOWN_EFFORT = 0.05
FOOT_EXTRAPOLATION = 5.0 
FOOT_LIFT_HEIGHT = 0.3

robot_list = []
def init(cont):
    if not cont.sensors[0].positive:
        return
    cont.owner['legs'] = []
    cont.owner.scene.pre_draw.append(update_ik)
    for child in cont.owner.children:
        if child['type'] == 'LEG':
            cont.owner['legs'].append(Leg(cont.owner, child))
    robot_list.append(cont.owner)
    cont.script = __name__ + '.update'
    
    
def update(cont):
    if not cont.sensors[0].positive:
        return
    vec = get_input()
    move_robot(cont.owner, vec)

def move_robot(robot, vec):
    lin = vec.xy
    lin.length = LIN_SPEED/bge.logic.getAverageFrameRate()
    ang = vec.z
    ang = min(ang, ROT_SPEED/bge.logic.getAverageFrameRate())
    ang = max(ang, -ROT_SPEED/bge.logic.getAverageFrameRate())
    
    robot.applyMovement(lin.to_3d(), True)
    robot.applyRotation([0,0,ang], True)
    leg_comforts = []
    can_move_new_leg = True
    for leg in robot['legs']:
        leg.robot_moved(lin, ang)
        leg_comforts.append((leg.get_comfort(), leg))
        if not leg.on_ground:
            can_move_new_leg = False
    if can_move_new_leg:
        leg = sorted(leg_comforts, key=lambda c: c[0])[-1]
        leg[1].pick_up()
     

def update_ik():
    for robot in robot_list:
        for leg in robot['legs']:
            leg.update_ik()

def get_key(key_str):
    active_keys = bge.logic.keyboard.events
    key_code = bge.events.__dict__[key_str]
    return active_keys[key_code]

def get_input():
    vec = mathutils.Vector([0,0,0])
    vec.x += get_key('WKEY') - get_key('SKEY')
    vec.y += get_key('DKEY') - get_key('AKEY')
    vec.z += get_key('QKEY') - get_key('EKEY')
    return vec

class Leg(object):
    '''Represents a 3DOF robot leg'''
    def __init__(self, robot, hip):
        self.robot = robot
        self.hip = hip
        self.thigh = hip.children[0]
        self.shin = self.thigh.children[0]
        self.foot = self.shin.children[0]
        self.foot.setParent(self.robot)
        self.rest_pos = self.get_foot_from_hip_root().copy()
        self.on_ground = True
        
    def height_sensor(self):
        '''Returns the height of the foot above the ground'''
        dir = self.foot.getAxisVect([0,0,-2])
        _, hitpoint, _ = self.foot.rayCast(self.foot.worldPosition+dir, self.foot.worldPosition-dir, 10)
        #bge.render.drawLine(self.foot.worldPosition+dir, self.foot.worldPosition-dir, [1,0,0])
        return (self.foot.worldPosition - hitpoint).z - 0.01
        
        
    def robot_moved(self, lin, ang):
        '''Moves the foot to accomadate a robot body motion'''
        height = self.height_sensor()
        
        linear = mathutils.Matrix.Translation(-lin.to_3d())
        linear.translation.z = -height/2
        angular = mathutils.Matrix.Rotation(-ang, 4, 'Z')
        old_pos = mathutils.Matrix.Translation(self.get_foot_in_robot_coords().translation)
        angular_robot = (old_pos * angular.inverted() * old_pos.inverted())
        trans =  angular * linear * angular_robot * self.get_foot_in_robot_coords()
        #trans =  angular * linear * self.get_foot_in_robot_coords()
        
        
        if self.on_ground:
            self.set_foot_in_robot_coords(trans)
        else:
            dir = self.get_move_dir(trans)
            dir.length = min(dir.length, LEG_SPEED/bge.logic.getAverageFrameRate())
            dir.z = (FOOT_LIFT_HEIGHT - height)/2
            delta = mathutils.Matrix.Translation(dir)
            self.set_foot_in_robot_coords(delta * self.get_foot_in_robot_coords())
            if self.get_comfort(trans) < PUT_DOWN_EFFORT:
                self.on_ground = True
    
    def pick_up(self):
        '''Picks up a leg so it is no longer on the ground'''
        if self.get_comfort() > PICK_UP_EFFORT:
            self.on_ground = False
    
    def get_foot_in_robot_coords(self):
        robot_frame = self.robot.worldTransform
        foot_frame = self.foot.worldTransform
        return robot_frame.inverted()*foot_frame
    
    def get_foot_from_hip_root(self):
        hip_frame = self.robot.worldTransform
        hip_frame.translation = self.hip.worldTransform.translation
        foot_frame = self.foot.worldTransform
        return hip_frame.inverted()*foot_frame

    def set_foot_in_robot_coords(self, new_pos):
        #print(new_pos)
        robot_frame = self.robot.worldTransform
        self.foot.worldTransform = robot_frame * new_pos

    def thigh_from_hip(self):
        return self.hip.worldTransform.inverted() * self.thigh.worldTransform
    def shin_from_thigh(self):
        return self.thigh.worldTransform.inverted() * self.shin.worldTransform
    
    def get_comfort(self, direction=None):
        diff = (self.get_foot_from_hip_root ()- self.rest_pos).translation
        #if direction != None:
        #    diff += direction.translation
        return diff.xy.length
    
    def get_move_dir(self, direction):
        diff = (self.get_foot_from_hip_root ()- self.rest_pos).translation
        #if direction != None:
        #    diff += direction.translation
        #diff.z = 0
        return -diff
    
    def update_ik(self):
        target = self.get_foot_from_hip_root().translation
        x = target.x
        y = target.y
        z = -target.z
        
        d2 = self.thigh_from_hip().translation[0]
        d3 = self.shin_from_thigh().translation[0]
        a1 = 0.1
        a2 = 0.4
        a3 = 0.75
        
        try:
            r_dash = math.sqrt(x**2 + y**2)
            sigma = math.asin((d2 + d3)/r_dash)
            theta1 = math.atan2(y, x) + sigma
            r = math.sqrt(r_dash**2 - (d2 + d3)**2)
            
            L = math.sqrt((r - a1)**2 + z**2)
            Beta = math.atan2(z, r - a1)
            theta2 = math.acos((a2**2 + L**2 - a3**2)/(2 * a2 * L)) - Beta
            theta3 = -math.pi/2 + math.acos((a2**2 + a3**2 - L**2)/(2*a2*a3))
            self.hip.localOrientation = [0,0,-math.pi/2+theta1]
            self.thigh.localOrientation = [theta2,0,0]
            self.shin.localOrientation = [theta3,0,0]
        except ValueError:
            pass