\documentclass[a4paper]{article}

\usepackage[a4paper, margin=1in]{geometry}  % Margins

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage[pdftex]{graphicx}
\usepackage{stfloats}
\DeclareGraphicsExtensions{.pdf, .jpeg, .jpg, .png}
\usepackage{array}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}


\usepackage{listings}
\usepackage{color}
    \definecolor{codegreen}{rgb}{0,0.4,0}
    \definecolor{codegray}{rgb}{0.5,0.5,0.5}
    \definecolor{codepurple}{rgb}{0.58,0,0.82}
    \definecolor{backcolour}{rgb}{0.95,0.95,0.92}

    \lstdefinestyle{mystyle}{
        backgroundcolor=\color{backcolour},
        commentstyle=\color{codegreen},
        keywordstyle=\color{magenta},
        numberstyle=\tiny\color{codegray},
        stringstyle=\color{codepurple},
        basicstyle=\scriptsize,
        breakatwhitespace=false,
        breaklines=true,
        captionpos=b,
        keepspaces=true,
        numbers=left,
        numbersep=5pt,
        showspaces=false,
        showstringspaces=false,
        showtabs=false,
        tabsize=4,
        frame=single
    }

    \lstset{style=mystyle}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}


\begin{document}

%%% TITLE BLOCK %%%
\begin{titlepage}
~\\[3.0cm]
\begin{center}
\HRule \\[0.4cm]
\Huge{\textsc{Robotic Arm Design and Control}} \\
\HRule \\
\end{center}

\begin{multicols}{3}
\begin{flushleft}
Geoffrey Irons\\
\end{flushleft}

\begin{center}
\today\\
\end{center}

\begin{flushright}
ENMT 482 Arm Assignment
\end{flushright}
\end{multicols}
~\\[-1.1cm]


\noindent

\end{titlepage}
\tableofcontents
\newpage

\section{The Robot}
Several months ago I built a quadruped robot in my spare time. At the time
I did not successfully implement the inverse kinematics or control
software. In this assignment, a single leg shall be examined. The
setup used is quite typical for a simple walking robot's leg as
it is simple both to build and analyse. A photo of the system and
it's critical dimensions can be seen in Figure \ref{fig:geometry}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{images/geometry.png}
    \caption{A photo of the robot's leg and it's critical dimensions
    \label{fig:geometry}}
\end{figure}
Due to being actual physical hardware, there are rotation limits on each
joint. This could be caused either by the servo's limit (180 degrees) or
a mechanical block. One such mechanical block can be seen in the picture
in Figure \ref{fig:geometry} where the chamfered top of the leg prevents
the foot from extending to straight.

\section{DH Parameters and Frames}
A standard method of representing a robot is to use DH parameters. These
represent the distances and angles between the joints, the actual link
geometry is not a factor. The DH parameters for the robot are shown in Table
\ref{tab:dh_parameters} and the robot generated with these parameters is
plotted in Figure \ref{fig:matlab}. The reference frames for the leg are
shown in Figure \ref{fig:frames}.

\begin{table}[h]
\centering
\begin{tabular}{| c | l l l l |}
    \hline
    Link & $\theta$ & $d$       & $a$     & $\alpha$  \\
    \hline
    1  & 0.0        & 0.0     & 0.01    & $\pi/2$     \\
    2  & 0.0        & 0.02    & 0.04    & 0.0         \\
    3  & $-\pi/2$   & 0.015   & 0.075   & 0.0         \\
    \hline
\end{tabular}
\caption{DH Parameters in a normal stance}
\label{tab:dh_parameters}
\end{table}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{images/matlab.png}
    \caption{The DH parameters above plotted in matlab
    \label{fig:matlab}}
\end{figure}

\newpage
\section{Forward Kinematics}
The forward kinematics give the robot tool position given the joint
angles. It is found by creating transformation matrices for all the links and
joints before multiplying them all together. The derivation for
the general case of a single link and joint is shown below:
\begin{eqnarray}
^{i-1}_{i'}T &=& T_{z}(d_{i}) =  \begin{bmatrix}
1 & 0 & 0 & 0  \\
0 & 1 & 0 & 0  \\
0 & 0 & 1 & d_{i}\\
0 & 0 & 0 & 1\\
\end{bmatrix}\\
%Line
^{i'}_{i''}T &=& R_{z}(\theta_{i}) =  \begin{bmatrix}
\cos{\theta_{i}} & -\sin{\theta_{i}} & 0 & 0  \\
\cos{\theta_{i}} & \cos{\theta_{i}} & 0 & 0  \\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
\end{bmatrix}\\
%Line
^{i''}_{i'''}T &=& T_{x}(a_{i}) =  \begin{bmatrix}
1 & 0 & 0 & a_{i}  \\
0 & 1 & 0 & 0  \\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
\end{bmatrix}\\
%Line
^{i'''}_{i''''}T &=& R_{x}(\alpha_{i}) =  \begin{bmatrix}
1& 0 & 0 & 0 \\
0 & \cos{\alpha_{i}} & -\sin{\alpha_{i}} & 0  \\
0 & \cos{\alpha_{i}} & \cos{\alpha_{i}} & 0  \\
0 & 0 & 0 & 1 \\
\end{bmatrix}\\
%Line
^{i-1}_{i}T &=& T_{z}(\theta_{i})T_{z}(d_{i})T_{x}(a)R_{x}(\alpha)
\end{eqnarray}
For the robot being modelled here, the forward kinematics is:
\begin{eqnarray}
T =& T_{z}(\theta_{1})T_{z}(0)T_{x}(0.1)R_{x}(\pi/2)\\
   & T_{z}(\theta_{2})T_{z}(0.2)T_{x}(0.4)R_{x}(0)\\
   & T_{z}(\theta_{3})T_{z}(0.15)T_{x}(0.75)R_{x}(0)
\end{eqnarray}
It can be written out in full, but that is an exercise best left for the
motivated! It is implemented in matlab as:
\lstinputlisting[language=Octave, lastline=11]{../Code/forward_kinematics.m}

\newpage
\section{Inverse Kinematics}
Solving the inverse kinematics is significantly harder than solving the forward
kinematics, but due to the simple structure of this robot it can
be solved graphically by dividing the robot into two planar systems.
\subsection{Top-down system}

\begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{images/top-down.png}
\end{minipage}
\begin{minipage}{0.45\textwidth}
    \begin{eqnarray}
    r' &=& \sqrt{x^{2}+y^{2}}\\
    \sigma &=& asin(\frac{d_{2}+d_{3}}{r'})\\
    \bf{\theta_{1}} &=& atan2(y / x) + \sigma\\
    r &=& \sqrt{(r')^{2} - (d_{2}+d_{3}{r'})^{2}}
    \end{eqnarray}
\end{minipage}

\subsection{Along-leg system}
\begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{images/along-leg.png}
\end{minipage}
\begin{minipage}{0.45\textwidth}
    \begin{eqnarray}
    L &=& \sqrt{(r - a_{1})^{2} + (-z)^2}\\
    \beta &=& atan2(-Z / (r - a_{1}))\\
    \bf{\theta_{2}} &=& acos(\frac{a_{2}^{2} + L^{2} - a_{3}^{2}}{2a_{2}L}) - \beta\\
    \bf{\theta_{3}} &=& -\pi + acos(\frac{a_{2}^{2} + a_{3}^{2} - L^{2}}{2a_{1}a_{2}})
    \end{eqnarray}
\end{minipage}

\subsection{The complete system}
The IK for the whole system can be seen in Appendix \ref{app:IK}. In code it
is implemented as:
\lstinputlisting[language=Octave, lastline=17]{../Code/inverse_kinematics.m}

\newpage
\section{The Jacobian}
The Jacobian allows the forces on the end effector to be converted into
torques, and on more complex robotic systems, allows the end effector to be
moved without requiring the full inverse kinematics. The analytic solution
for the jacabian was too extensive to derive, but it can be implemented
numerically as:
\lstinputlisting[language=Octave]{../Code/jacobian.m}

One use for the Jacobian is to check the existence of singularities:
points where a small motion requires a large motion from the joints. This
is indicated when the jacobian is ill-conditioned (nearly singular). This was
evaluated at over 190,000 points within the robot's workspace to check for
these locations. This is shown in Figure \ref{fig:workspace}. The workspace
was determined by iterating over many joint angles and plotting the foot
position.
The colouring of the graph indicates the conditioning of each point, with
yellow being worse conditioned than blue. Despite the appearance of the
graph\footnote{Matlab's scatter3 command automatically scales colour
values} all of the points in the robots workspace were well conditioned as
the mechanical limitations of the system prevent the leg extending fully.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{images/workspace.png}
    \caption{The robots workspace, coloured by the conditioning of the jacobian
    \label{fig:workspace}}
\end{figure}

\newpage
\section{Walking}
The point of a robot leg is to walk. There are two ways to handle this. One
is to pre-define a path. The other is to teach the robot what walking is:
movement in a certain direction. This is done by giving each leg a region of
`comfort' centred on where the leg would normally rest. You then tell the
leg that when it's on the ground it should be moving in a certain direction.
When the leg reaches a certain level of 'discomfort,' the leg is picked up
and transits to it's rest position. To keep the leg's motion centred around
it's rest position, the region of comfort is shifted in the desired direction
of motion.

This is shown digramatically in Figure \ref{fig:walkcycle}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{images/walkcycle.png}
    \caption{The walkcycle algorithm
    \label{fig:walkcycle}}
\end{figure}


An extension to this algorithm is to add in an overseeing system that
looks at all the legs of the robot and moves the leg furthest away from it's
desired position. Due to some limitations with matlab's ability to plot
multiple robot arms, the analysis of the leg motion was continued in Blender.
A screenshot showing the path a single leg takes is shown in Figure
\ref{fig:leg_path}. The blend file is available on the assignment
repository\footnote{\url{https://gitlab.com/sdfgeoff/walking-robot-assignment}}.
A video is available at \url{https://www.youtube.com/watch?v=ZEtxEr\_ZzJY}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{images/leg_path.jpg}
    \caption{The leg path on a four leg system.
    \label{fig:leg_path}}
\end{figure}



\newpage
\begin{appendix}
\section{Complete IK Solution \label{app:IK}}
\begin{eqnarray}
\bf{\theta_{1}} &=& atan2(y / x) + asin(\frac{d_{2}+d_{3}}{\sqrt{x^{2}+y^{2}}})\\
%Line
\bf{\theta_{2}} &=& acos(\frac{a_{2}^{2} + (\sqrt{((\sqrt{(\sqrt{x^{2}+y^{2}})^{2} - (d_{2}+d_{3}{\sqrt{x^{2}+y^{2}}})^{2}}) - a_{1})^{2} + (-z)^2})^{2} - a_{3}^{2}}{2a_{2}(\sqrt{((\sqrt{(\sqrt{x^{2}+y^{2}})^{2} - (d_{2}+d_{3}{r'})^{2}}) - a_{1})^{2} + (-z)^2})})\\
  &&- atan2(-Z / (\sqrt{(\sqrt{x^{2}+y^{2}})^{2} - (d_{2}+d_{3}{\sqrt{x^{2}+y^{2}}})^{2}} - a_{1}))\\
%Line
\bf{\theta_{3}} &=& -\pi + acos(\frac{a_{2}^{2} + a_{3}^{2} - (\sqrt{(\sqrt{(\sqrt{x^{2}+y^{2}})^{2} - (d_{2}+d_{3}{\sqrt{x^{2}+y^{2}}})^{2}} - a_{1})^{2} + (-z)^2})^{2}}{2a_{1}a_{2}})
\end{eqnarray}


\newpage
\section{Code}
\subsection{robot.m}
\lstinputlisting[language=Octave]{../Code/robot.m}

\subsection{walk.m}
\lstinputlisting[language=Octave]{../Code/walk.m}

\subsection{inverse\_kinematics.m}
\lstinputlisting[language=Octave]{../Code/inverse_kinematics.m}

\subsection{forward\_kinematics.m}
\lstinputlisting[language=Octave]{../Code/forward_kinematics.m}

\subsection{jacobian.m}
\lstinputlisting[language=Octave]{../Code/jacobian.m}

\subsection{workspace.m}
\lstinputlisting[language=Octave]{../Code/workspace.m}
\end{appendix}

\end{document}
