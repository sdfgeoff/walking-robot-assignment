\select@language {english}
\contentsline {section}{\numberline {1}The Robot}{2}{section.1}
\contentsline {section}{\numberline {2}DH Parameters and Frames}{2}{section.2}
\contentsline {section}{\numberline {3}Forward Kinematics}{3}{section.3}
\contentsline {section}{\numberline {4}Inverse Kinematics}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Top-down system}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Along-leg system}{4}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}The complete system}{4}{subsection.4.3}
\contentsline {section}{\numberline {5}The Jacobian}{5}{section.5}
\contentsline {section}{\numberline {6}Walking}{6}{section.6}
\contentsline {section}{\numberline {A}Complete IK Solution }{7}{appendix.A}
\contentsline {section}{\numberline {B}Code}{8}{appendix.B}
\contentsline {subsection}{\numberline {B.1}robot.m}{8}{subsection.B.1}
\contentsline {subsection}{\numberline {B.2}walk.m}{8}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}inverse\_kinematics.m}{8}{subsection.B.3}
\contentsline {subsection}{\numberline {B.4}forward\_kinematics.m}{9}{subsection.B.4}
\contentsline {subsection}{\numberline {B.5}jacobian.m}{9}{subsection.B.5}
\contentsline {subsection}{\numberline {B.6}workspace.m}{10}{subsection.B.6}
